const express = require("express");
const app = express();
const port = 5000;
const cors = require("cors");
const bodyparser = require("body-parser");

app.use(cors());
app.use(bodyparser.urlencoded());
app.use(bodyparser.json());
 

let items = [{id: 1, nombre:"Albert", apellido: "Segura"}, {id: 2, nombre:"Jose", apellido: "Perez"}, {id: 3,nombre:"Maria", apellido: "Lopez"},
    
    {
        id: 4,
        nombre:"Kim",
        apellido: "Ramirez"
    }
]

app.get("/", (req, res) => {
    res.send(items);
})

app.post("/", (req, res) => {
    
  const lastId = items[items.length-1].id;
  req.body.id = lastId + 1;

  items.push(req.body);
    res.send(items);

})

app.delete("/", (req, res) => {
    
    const result = items.filter((item) => item.id !== req.body.data);
    items = result;
   

    // for (let index = 0; index < items.length; index++) {
        
    //     if(items[index].id === req.body){
    //         posicion++;
    //         return;
    //     }
    //     posicion++;
    // }

    // items.pop(posicion);
    res.send(items);

})


app.put("/", (req, res) => {

     const resultItem = items.find((item) => item.id === req.body.id);
     const result = items.filter((item) => item.id !== req.body.id);
     items = result;

    const newObj = {
        id: req.body.id,
        nombre: req.body.nombre,
        apellido: req.body.apellido
    }

    items.push(newObj);

    res.send(items);

  })


app.listen(port, () => {
    console.log("App corriendo el puerto " + port);
});
